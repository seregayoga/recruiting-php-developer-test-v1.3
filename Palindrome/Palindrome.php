<?php

class Palindrome
{
    /**
     * @param string $word
     * @return bool
     */
    public static function isPalindrome($word)
    {
        $word = strtolower($word);
        $word = str_replace(' ', '', $word);

        $chars = str_split($word);
        $len = count($chars);
        $mid = count($chars) / 2;
        for ($i = 0, $j = $len - 1; $i < $mid && $j > $mid; $i++, $j--) {
            if ($chars[$i] !== $chars[$j]) {
                return false;
            }
        }

        return true;
    }
}