<?php

class OddNumber
{
    /**
     * @var int[]
     */
    private $numbers;

    /**
     * @param array $numbers
     */
    public function __construct(array $numbers)
    {
        $this->numbers = $numbers;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function find()
    {
        $odd = [];
        foreach ($this->numbers as $number) {
            if (isset($odd[$number])) {
                unset($odd[$number]);
            } else {
                $odd[$number] = true;
            }
        }

        if (empty($odd)) {
            throw new Exception('Could not find a number');
        }
        return key($odd);
    }
}