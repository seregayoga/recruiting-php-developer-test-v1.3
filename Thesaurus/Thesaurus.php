<?php

class Thesaurus
{
    /**
     * @var array
     */
    private $thesaurus;

    /**
     * @param array $thesaurus
     */
    function __construct($thesaurus)
    {
        $this->thesaurus = $thesaurus;
    }

    /**
     * @param $word
     * @return false|string
     */
    public function getSynonyms($word)
    {
        return json_encode([
            'word' => $word,
            'synonyms' => $this->thesaurus[$word] ?? [],
        ]);
    }
}