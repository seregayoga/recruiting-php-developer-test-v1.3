<?php

class FileOwners
{
    /**
     * @param array $files
     * @return array
     */
    public static function groupByOwners($files)
    {
        $filesByOwners = [];

        foreach ($files as $file => $name) {
            $filesByOwners[$name][] = $file;
        }

        return $filesByOwners;
    }
}